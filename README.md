# ESP32 Tools


Repo for various ESP32 things that I will forget if I don't put them somewhere.  If you want to try more complex projects check out [Random Nerd Tutorials](https://randomnerdtutorials.com/projects-esp32-cam/) for a fantastic set of guides.

## Arduino IDE / PlatformIO Setup

 * Installing and initial configuration of the Arduino IDE or PlatformIO on VS Code is left as an exercise for the reader.  I will instead focus on the relevant bits that proved to be common pitfalls when pushing code to the boards.

 *  **You might find references to resetting the board or holding the BOOT button when uploading.  This should *not* be required for these boards**

### Install the ESP32 Board Library

 1. Follow the instructions on [the Espressif docs](https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html) **and stop on the final step. DO NOT INSTALL THE LATEST VERSION.**  
 2. Install v1.0.6 of the library

    ![v1.0.6](images/v3x-select-106-ide-core.png)

    See the [easytarget repo](https://github.com/easytarget/esp32-cam-webserver/blob/3.x/README.md#downgrade-the-esp-arduino-core-to-v106) for more, and [this discussion](https://github.com/easytarget/esp32-cam-webserver/issues/191#issuecomment-1061687839) for context as to why the latest version will not work.

### Configure IDE

1. Connect the board
2. Select your port, something like `COM4` on Windows or `/dev/ttyUSB0` on Linux
3. Select the `AI Thinker ESP32-CAM` board from the ESP32 board submenu
   ![board_selection](images/board_selection.jpg)
4. Set `CPU Frequency` to `240MHz`
5. Set `Flash Frequency` to `80MHz`
6. Set `Flash Mode` to `QIO`
7. (Optional) Older versions of the IDE allow you to select the partioning scheme.  If you have this, it will be under Tools > Partition Scheme, select “Huge APP (3MB No OTA)“. 
8. Upload a sketch to ensure communication, see the Blink sketch below.

---

## [Blink](https://gitlab.com/ss32/esp32/-/tree/main/blink)

A simple sketch to ensure operation of the board, shamelessly taken from [Random Nerd Tutorials](https://randomnerdtutorials.com/projects-esp32/).  Follow the setup above and you should see the LED on the front of the board blinking after uploading the sketch.

![blink](images/blink.jpg)

## [Camera Web Server](https://gitlab.com/ss32/esp32/-/tree/main/CameraWebServer)

Slightly modified version of the [arduino-esp32](https://github.com/espressif/arduino-esp32/tree/master/libraries/ESP32/examples/Camera/CameraWebServer) example to work with the AI Thinker camera module.  

**Note:** Make sure to modify the [SSID and password](https://gitlab.com/ss32/esp32/-/blob/main/CameraWebServer/CameraWebServer.ino#L22-23) to match your local network. If successful you should see something like this in the terminal.  **Note:** You may need to reset the board with the USB connected in order to see the initial printout of the wifi connection.

![webserver](images/server.png)

Pull up a browser and nagivate to the IP listed in the terminal where you'll be able to play with camera settings.  This camera module is good up to the maximum resolution of 1600x1200.

![cameraStill](images/camstill.png)

---

## [DHT Monitor](https://gitlab.com/ss32/esp32/-/tree/main/espDHT)

Simple sensor interface and web server to monitor temperature and humidity as well as Python script to query and plot data over time.

![plants](images/plantweather.jpg)
![temp_humidity_ts](espDHT/temp_humidity_plot.png)