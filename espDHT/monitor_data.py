import requests
import matplotlib.pylab as plt
import time
import argparse
import datetime
import matplotlib.patheffects as pe
from matplotlib.ticker import MaxNLocator


server = 'http://192.168.1.2'


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--plot',action="store_true", help='Plot data')
    parser.add_argument('--dt', type=int, default=300, help="Interval in seconds to wait between readings")
    return parser.parse_args()

def get_value_from_server(server: str, value: str) -> float:
    raw_value = requests.get(f"{server}/{value}").text
    try:
        x = float(raw_value)
    except ValueError:
        x = -1
    return x

def moving_average(data, window_size):
    moving_averages = []
    for i in range(len(data) - window_size + 1):
        this_window = data[i : i + window_size]
        window_average = sum(this_window) / window_size
        moving_averages.append(window_average)
    return moving_averages

def convert_unix_nanoseconds_to_datetime(nano_str):
    unix_timestamp = int(nano_str) / 1e9
    dt = datetime.datetime.fromtimestamp(unix_timestamp)
    return dt

def plot_data(path: str):
    with open(path, 'r') as infile:
        lines = infile.readlines()
    # Assumes lines are of format [time,temperature,humidity] where time is unix nanoseconds
    temperature = []
    humidity = []
    times = []
    for l in lines:
        vals = l.strip().split(',')
        times.append(convert_unix_nanoseconds_to_datetime(vals[0]))
        temperature.append(float(vals[1]))
        humidity.append(float(vals[2]))
    
    plt.style.use('dark_background')
    _, ax1 = plt.subplots()
    window_size = 12
    temp_avg = moving_average(temperature, window_size)
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Temperature (F)',color='lightgreen')
    ax1.plot(times,temperature,'.',color='lightgreen',linewidth=3)
    plt.plot(times[window_size-1:],temp_avg, 'w--')
    ax1.tick_params(axis='y',labelcolor='white')
    ax1.set_ylim([50,100])
    ax2 = ax1.twinx()
    humidity_avg = moving_average(humidity, window_size)
    ax2.set_ylabel('Humidity (%)',color='lightblue')
    ax2.plot(times,humidity,'.',color='lightblue',linewidth=3)
    plt.plot(times[window_size-1:],humidity_avg,'w--')
    ax2.tick_params(axis='y',labelcolor='white')
    ax2.set_ylim([50,100])
    plt.gca().xaxis.set_major_locator(MaxNLocator(nbins=5))
    plt.savefig('temp_humidity_plot.png',dpi=200)

def log_data(server:str, logfile: str):
    temp = get_value_from_server(server, 'temperature')
    humidity = get_value_from_server(server, 'humidity')
    t = time.time_ns()
    if (temp == -1 or humidity == -1):
        return
    with open(logfile,'a') as log:
        datastr = f"{t},{temp},{humidity}\n"
        log.write(datastr)
        print(datastr.strip())
    return

args = get_args()
logfile = 'datalog.txt'
if args.plot:
    print('Plotting data')
    plot_data(logfile)
else:
    print(f'Logging data from {server}')
    while True:
        try:
            log_data(server, logfile)
            time.sleep(args.dt)
        except KeyboardInterrupt:
            print('Closing...')
            exit(0)
