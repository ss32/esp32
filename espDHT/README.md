## DHT Monitor

Uses an ESP32 + DHT22 to post temperature and humidity readings to a web server running on the ESP32.  Also included is a python script for monitoring and plotting the readings to keep track of them over time.

Be sure to set `ssid` and `password` in `espDHT.ino` to your local network.  You will need to check the IP address via the serial console at first boot to ensure it connected.

![webpage](../images/plantweather.jpg)

### Python Monitoring

`monitor_data.py` will query the web server and log readings to `datalog.txt`.  Passing `--plot` produces the plot seen below.

The default interval is 300 seconds and can be set via the `--dt` argument.  

```bash
python3 monitor_data.py --dt 500
```

There are multple ways to continously log data, such as using a `cronjob` that executes at a prescribed interval, but the easiest is to just leave the script running in a terminal window, ideally in a `tmux` or `screen` session that can be disconnected and run in the background.  This is left to an exercise for the reader. Again, be sure to set the `server` value in the code so it looks for the ESP at the right address.

The data plot shows both individual readings as dots and a moving average of the last 12 readings (1 hour at the default sample interval).  In the plot below I lost connectivity overnight and had to reset the server.

![temp_humidity_ts](../espDHT/temp_humidity_plot.png)